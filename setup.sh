#!/bin/sh
#if [ $SOMAXCONN ];then
	#	echo $SOMAXCONN > /proc/sys/net/core/somaxconn;
	#	echo $SOMAXCONN > /proc/sys/net/ipv4/tcp_max_syn_backlog
#fi
export MY_IP=$(hostname -i | awk '{ print $1 }');
echo "setting up service: ${SERVICE}";
consul-template -consul=${CONSUL_URL} -template="/root/haproxy.ctmpl:/usr/local/etc/haproxy/haproxy.cfg:/bin/sh restart_haproxy.sh";
