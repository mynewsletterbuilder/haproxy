.PHONY: docker

IMAGE_BASE = jbanetwork/
IMAGE = haproxy
MY_PWD = $(shell pwd)

all: php

php:
	docker build -t $(IMAGE_BASE)$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
ifneq (,$(findstring p,$(MAKEFLAGS)))
	docker push $(IMAGE_BASE)$(IMAGE)
endif