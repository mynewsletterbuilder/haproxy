FROM haproxy:alpine

ENV CONSUL_TEMPLATE_VERSION 0.14.0

RUN apk add --update --no-cache wget unzip mysql-client && \
    wget --no-check-certificate https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip -O /tmp/consul-template.zip && \
    unzip /tmp/consul-template.zip -d /usr/local/bin && \
    rm -rf /tmp/consul-template.zip && \
    apk del wget unzip

COPY setup.sh /root/setup.sh
COPY restart_haproxy.sh /root/restart_haproxy.sh
COPY haproxy.ctmpl /root/haproxy.ctmpl

WORKDIR /root

CMD /bin/sh setup.sh
