#!/usr/bin/env bash

echo "restarting haproxy...";
cat /usr/local/etc/haproxy/haproxy.cfg;
echo "";
#TODO: this should be in a loop that tries to kill until it is dead
if pidof haproxy;then
    echo "trying to kill pid: $(pidof haproxy)";
    kill $(pidof haproxy);
fi
haproxy -f /usr/local/etc/haproxy/haproxy.cfg ;
echo "running with pid: $(pidof haproxy)";
