## jbanetwork/haproxy
## load balancer

takes 4 ENV variables to config:
**CONSUL_URL=10.0.0.113:8500** - the ip and port of the consul server (or consul load balancer)

**SERVICE=postfix-test** - the service is the shortest possible version of the image of the containers that we want to grab ports for and load balance.

**HA_MODE=tcp** - see haproxy docs for node options... unless you are doing http.. you should stick to tcp
**HA_PORT=25** - the port to listen on

you should also add an alias so you don't have to figure out what ip is the active server.

check out `docker/mm/lb.docker-compose.yml` for examples